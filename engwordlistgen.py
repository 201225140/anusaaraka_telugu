import re
f=open('engtel_calts_dict.txt','r')
for line in f:
	a=re.split(':',line)
	if len(a) ==3 :
		if a[1] == 'saM.' or a[1] == 'num.':
			a[1] = 'number'
		elif a[1]=='kri.' or a[1]=='v.' or a[1]=='vt.' or a[1]=='VI.' or a[1]=='vn.' or a[1]=='k.' or a[1]=='va.' or a[1]=='vpl.' or a[1]=='vpt.':
			a[1]='verb'
		elif a[1] == 'vi.' or a[1]=='adj.':
			a[1] = 'adjective'
		elif a[1]=='nA.' or a[1]=='n.' or a[1]=='n-sci.' :
			a[1] = 'noun'
		elif a[1]=='krivi.' or a[1]=='adv.' or a[1]=='det.' or a[1]=='adverb.' :
			a[1] = 'adverb'
		elif a[1] =='sanA.' or a[1]=='P.' or a[1]=='pn.' or a[1]=='Pro.' or a[1]=='Interro.' or a[1]=='pron.' :
			a[1] = 'pronoun'
		elif a[1]=='a.' or a[1]=='dem.' :
			a[1] = 'determiner'
		elif a[1]=='interj.' or a[1]=='I.' or a[1]=='Int.':
			a[1] = 'interjection'
		elif a[1] =='p.' or a[1]=='PP.' :
			a[1] = 'preposition'
		elif a[1] =='PropN.' or a[1]=='PN.':
			a[1] = 'PropN'
		elif a[1] =='Abb.' or a[1]=='Abbr.':
			a[1] = 'ABBR'
		elif a[1] =='Conj.' or a[1]=='conj.':
			a[1] = 'conjunction'
		elif a[1] =='Pref.' or a[1]=='Pre.':
			a[1] = 'PREFIX'
		elif a[1] =='Interro.':
			a[1] = 'wh-adverb'
		elif a[1] =='vl.' or a[1]=='Phr.' or a[1]=='na.' or a[1]=='vs.' or a[1]=='indcl.':
			a[1] = 'UNDEFINED'
		a[2] = re.split('/',a[2])
		j=0
		while j < len(a[2]):
			i=a[2][j]
			if len(i)>2:
				if i[0] == '1' or i[0] == '2' or i[0] == '3' or i[0] == '4' or i[0] == '5' or i[0] == '6' or i[0] == '7' or i[0] == '8' or i[0] == '9':
					if i[1] == '.':
						i=i[2:]
			i=re.split(' ',i)
			i='_'.join(i)	
			a[2][j]=i			
			j+=1		
						
		a[2] = '/'.join(a[2])
		i =  a[0]
		j=0
		while j<=3:
			if i[0]==' ' or i[0] == '1' or i[0] == '2' or i[0] == '3' or i[0] == '4' or i[0] == '5' or i[0] == '6' or i[0] == '7' or i[0] == '8' or i[0] == '9' or i[0]=='.':
				i=i[1:]
			j+=1

		a[0]=i
		a[0] = re.split('_',a[0])
		a[0]='-'.join(a[0])
		a[0] = re.split(' ',a[0])
		a[0]='-'.join(a[0])		
		import sys
		sys.stdout.write(a[0]+'_'+a[1]+'\t'+a[2])
	