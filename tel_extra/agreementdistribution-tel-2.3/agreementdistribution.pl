#!/usr/bin/perl


use Getopt::Long;
GetOptions("help!"=>\$help,"path=s"=>\$vibh_home,"input=s"=>\$input,"output=s",\$output,"rulefile=s",\$ruleFile);
print "Unprocessed by Getopt::Long\n" if $ARGV[0];
foreach (@ARGV) {
	print "$_\n";
	exit(0);
}

if($help eq 1)
{
	print "agreement Distribution  Version 1.1\n(17 th Oct 2008 )\n\n";
	print "usage : perl agreementdistribution.pl --path=\$PWD [-i inputfile|--input=\"input_file\"] [-o outputfile|--output=\"output_file\"] --rule=hin/rule.rl\n";
	print "\tIf the output file is not mentioned then the output will be printed to STDOUT\n";
	exit(0);
}

if($vibh_home eq "")
{
	print "Please Specify the Path as defined in --help\n";
	exit(0);

}
if($ruleFile eq "")
{
	print "Please Specify the Path of the RULE FILE\n";
	exit(0);

}

require "$vibh_home/API/shakti_tree_api.pl";
require "$vibh_home/API/feature_filter.pl";

open(RULE,$ruleFile);
@rules = <RULE>;

%rule_hash = ();

foreach $rule (@rules)
{
	if($rule !~ /^\s*$/)
	{
		chomp($rule);
		($fea,$array_in) = split(/\s+/,$rule);
		$rule_hash{ $fea } .= $array_in."_";
	}
}


#print "here";

&read_story($input);

$numBody = &get_bodycount();
for(my($bodyNum)=1;$bodyNum<=$numBody;$bodyNum++)
{

	$body = &get_body($bodyNum,$body);

	my($numPara) = &get_paracount($body);

	for(my($i1)=1;$i1<=$numPara;$i1++)
	{

		my($para);
		$para = &get_para($i1);


		my($numSent) = &get_sentcount($para);

		for(my($j1)=1;$j1<=$numSent;$j1++)
		{
			my($sent) = &get_sent($para,$j1);
			my @all_children =&get_nodes(3,"VGF",$sent);
			$num = @all_children;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VGF",$sent);
				$node = $all_children[$i];
				my $val_fs=&get_field($node, 4,$sent);
				$fs_array = &read_FS($val_fs,$sent);

				@lexs = &get_values("lex", $fs_array,$sent);
				@cats = &get_values("cat", $fs_array,$sent);
				@tams = &get_values("vib", $fs_array,$sent);
				@gens = &get_values("gen", $fs_array,$sent);
				@nums = &get_values("num", $fs_array,$sent);
				@pers = &get_values("per", $fs_array,$sent);

				$gen = $gens[0];
				$num = $nums[0];
				$per = $pers[0];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;



				while()
				{
					$child = $childs[$num_child-1];
					my $pos = &get_field($child,3,$sent);
					if($pos ne "VAUX")
					{
						$num_child--;
					}
					else
					{
						last;
					}
					if($num_child == 0)
					{
						last;
					}
				}
				if($num_child != 0)
				{
					my @gen_chunk_arr=();
					push @gen_chunk_arr,$gen;

					my @num_chunk_arr=();
					push @num_chunk_arr,$num;

					my @per_chunk_arr=();
					push @per_chunk_arr,$per;

					@array = split(/\_/,$rule_hash{"gen"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"per"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"num"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}
				}
			}
			my @all_children =&get_nodes(3,"VNN",$sent);
			$num = @all_children;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VNN",$sent);
				$node = $all_children[$i];
				my $val_fs=&get_field($node, 4,$sent);
				$fs_array = &read_FS($val_fs,$sent);

				@lexs = &get_values("lex", $fs_array,$sent);
				@cats = &get_values("cat", $fs_array,$sent);
				@tams = &get_values("vib", $fs_array,$sent);
				@gens = &get_values("gen", $fs_array,$sent);
				@nums = &get_values("num", $fs_array,$sent);
				@pers = &get_values("per", $fs_array,$sent);

				$gen = $gens[0];
				$num = $nums[0];
				$per = $pers[0];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;



				while()
				{
					$child = $childs[$num_child-1];
					my $pos = &get_field($child,3,$sent);
					if($pos ne "VAUX")
					{
						$num_child--;
					}
					else
					{
						last;
					}
					if($num_child == 0)
					{
						last;
					}
				}
				if($num_child != 0)
				{
					my @gen_chunk_arr=();
					push @gen_chunk_arr,$gen;

					my @num_chunk_arr=();
					push @num_chunk_arr,$num;

					my @per_chunk_arr=();
					push @per_chunk_arr,$per;

					@array = split(/\_/,$rule_hash{"gen"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"per"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"num"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}
				}
			}
			my @all_children =&get_nodes(3,"VGINF",$sent);
			$num = @all_children;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VGINF",$sent);
				$node = $all_children[$i];
				my $val_fs=&get_field($node, 4,$sent);
				$fs_array = &read_FS($val_fs,$sent);

				@lexs = &get_values("lex", $fs_array,$sent);
				@cats = &get_values("cat", $fs_array,$sent);
				@tams = &get_values("vib", $fs_array,$sent);
				@gens = &get_values("gen", $fs_array,$sent);
				@nums = &get_values("num", $fs_array,$sent);
				@pers = &get_values("per", $fs_array,$sent);

				$gen = $gens[0];
				$num = $nums[0];
				$per = $pers[0];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;



				while()
				{
					$child = $childs[$num_child-1];
					my $pos = &get_field($child,3,$sent);
					if($pos ne "VAUX")
					{
						$num_child--;
					}
					else
					{
						last;
					}
					if($num_child == 0)
					{
						last;
					}
				}
				if($num_child != 0)
				{
					my @gen_chunk_arr=();
					push @gen_chunk_arr,$gen;

					my @num_chunk_arr=();
					push @num_chunk_arr,$num;

					my @per_chunk_arr=();
					push @per_chunk_arr,$per;

					@array = split(/\_/,$rule_hash{"gen"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"per"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"num"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}
				}
			}
			my @all_children =&get_nodes(3,"VNGF",$sent);
			$num = @all_children;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VNGF",$sent);
				$node = $all_children[$i];
				my $val_fs=&get_field($node, 4,$sent);
				$fs_array = &read_FS($val_fs,$sent);

				@lexs = &get_values("lex", $fs_array,$sent);
				@cats = &get_values("cat", $fs_array,$sent);
				@tams = &get_values("vib", $fs_array,$sent);
				@gens = &get_values("gen", $fs_array,$sent);
				@nums = &get_values("num", $fs_array,$sent);
				@pers = &get_values("per", $fs_array,$sent);

				$gen = $gens[0];
				$num = $nums[0];
				$per = $pers[0];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;



				while()
				{
					$child = $childs[$num_child-1];
					my $pos = &get_field($child,3,$sent);
					if($pos ne "VAUX")
					{
						$num_child--;
					}
					else
					{
						last;
					}
					if($num_child == 0)
					{
						last;
					}
				}
				if($num_child != 0)
				{
					my @gen_chunk_arr=();
					push @gen_chunk_arr,$gen;

					my @num_chunk_arr=();
					push @num_chunk_arr,$num;

					my @per_chunk_arr=();
					push @per_chunk_arr,$per;

					@array = split(/\_/,$rule_hash{"gen"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"per"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"num"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}
				}
			}
			my @all_children =&get_nodes(3,"VGNN",$sent);
			$num = @all_children;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VGNN",$sent);
				$node = $all_children[$i];
				my $val_fs=&get_field($node, 4,$sent);
				$fs_array = &read_FS($val_fs,$sent);

				@lexs = &get_values("lex", $fs_array,$sent);
				@cats = &get_values("cat", $fs_array,$sent);
				@tams = &get_values("vib", $fs_array,$sent);
				@gens = &get_values("gen", $fs_array,$sent);
				@nums = &get_values("num", $fs_array,$sent);
				@pers = &get_values("per", $fs_array,$sent);

				$gen = $gens[0];
				$num = $nums[0];
				$per = $pers[0];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;



				while()
				{
					$child = $childs[$num_child-1];
					my $pos = &get_field($child,3,$sent);
					if($pos ne "VAUX")
					{
						$num_child--;
					}
					else
					{
						last;
					}
					if($num_child == 0)
					{
						last;
					}
				}
				if($num_child != 0)
				{
					my @gen_chunk_arr=();
					push @gen_chunk_arr,$gen;

					my @num_chunk_arr=();
					push @num_chunk_arr,$num;

					my @per_chunk_arr=();
					push @per_chunk_arr,$per;

					@array = split(/\_/,$rule_hash{"gen"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"per"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}

					@array = split(/\_/,$rule_hash{"num"});
					foreach $la (@array)
					{
						if($la ne "")
						{
							$index = $num_child + $la;
							$child = $childs[$index];
							my $head_node=&get_field($child,4,$sent);
							my $pos = &get_field($child,3,$sent);
							if($pos eq "VAUX")
							{
								my $FSreference1 = &read_FS($head_node,$sent);
								&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
								my $string=&make_string($FSreference1,$sent);
								&modify_field($child,4,$string,$sent);
							}
						}
					}
				}
			}
		}
	}
}
if($output eq "" )
{
	&printstory();
}

else
{
	&printstory_file("$output");
}

