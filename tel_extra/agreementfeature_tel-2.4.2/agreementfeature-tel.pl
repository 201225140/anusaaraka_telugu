#!/usr/bin/perl

use Log::Log4perl;
use Getopt::Long;
GetOptions("help"=>\$oHelp,"input=s"=>\$filename,"logFile=s"=>\$logFile,"version"=>\$version,"path=s"=>\$path,"male=s"=>\$male_file,"female=s"=>\$female, "human=s"=>\$human);
$SEN_GEN_HOME = $path;

if($oHelp or $path eq "")
{
	print "Usage : perl agreementfeature.pl -i input_SSF --path=/home/agreementfeature --male=male --female=femlae db path\n";
	exit(0);
}
if($version)
{
	print "Agreemet feature v1.1\n";
	exit(0);
}

require "$SEN_GEN_HOME/API/shakti_tree_api.pl";
require "$SEN_GEN_HOME/API/feature_filter.pl";
my $logfile = "$SEN_GEN_HOME/common/log.conf";
Log::Log4perl->init($logfile);
$log = Log::Log4perl->get_logger;

sub logfile {
	my $myLog = $options{$logFile} || "agreementfeature.log";
	return $myLog;
}

open(FILE_male,"$male_file");

%mn = ();
@males = <FILE_male>;
foreach $male (@males)
{
	chomp($male);
	$key=$mn{$male}++;
}

open(FILE_hum,"$human");

%hum = ();
@humas = <FILE_hum>;
foreach $hume (@humas)
{
	chomp($hume);
	$key=$hum{$hume}++;
}

open(FILE_female,"$female");

%fmn = ();
@females = <FILE_female>;
foreach $female (@females)
{
	chomp($female);
	$key=$fmn{$female}++;
}

sub sen_gen()
{

	&read_story($filename);

	$numBody = &get_bodycount();
	for(my($bodyNum)=1;$bodyNum<=$numBody;$bodyNum++)
	{

		$body = &get_body($bodyNum,$body);

		my($numPara) = &get_paracount($body);

		for(my($i1)=1;$i1<=$numPara;$i1++)
		{

			my($para);
			$para = &get_para($i1);
			my($numSent) = &get_sentcount($para);
			for(my($j1)=1;$j1<=$numSent;$j1++)
			{
				my($sent) = &get_sent($para,$j1);
				my @all_children=&get_nodes(3,"NP",$sent);

				@nodes1=&get_leaves($sent);
				@nodes=(@nodes1,@all_children);

				foreach $node (@nodes)
				{
					$child = $node; 
					my $pos=&get_field($child, 3,$sent);
					my $temp_fs_4 = &get_field($child, 4,$sent);
					my $temp_fs = &read_FS($temp_fs_4,$sent);
					my @root = &get_values("lex",$temp_fs,$sent);
					my @lcat = &get_values("cat",$temp_fs,$sent);
					my @gender = &get_values("gen",$temp_fs,$sent);
					my @num = &get_values("num",$temp_fs,$sent);
					my @per = &get_values("per",$temp_fs,$sent);
					my @suf = &get_values("tam",$temp_fs,$sent);
					my $temp_fs_2=&get_field($child, 2,$sent);
#					print $temp_fs_2." HERE\n";i
					if($pos eq "NN" or $pos eq "NNP" or $pos eq "PRP" or $pos eq "NP" or $pos eq "WQ") {
						foreach $root(@root) {
							foreach $num(@num) {
								if($num eq "sg") {

#if($mn{$temp_fs_2})
									if(($mn{$root})||($root=~/du$/)||(($lcat[0] eq "pn")&&($gender[0] eq "m"))) {
										$new_gender[0] = "m";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
									elsif($fmn{$root}) {
										$new_gender[0] = "f";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
									elsif($hum{$root}) {
										$new_gender[0] = "fm";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
									elsif(($suf[0]=~/interr/)&&(($pos eq "WQ")||($pos eq "NP"))) {
										$new_gender[0] = "fm";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
if($root eq "eVvaru"){
$new_num[0]="pl";
										&update_attr_val("num",\@new_num,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
my @new_num;
}
									}
	else
									{
#changed from "fn" to "n" 05052009 
#$new_gender[0] = "fn";
										$new_gender[0] = "n";
#hyding because it changing masculine as  neuter	sm-1607					
				&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}



								}
								elsif($num eq "pl") {
#if($mn{$temp_fs_2})
							if(($mn{$root})||(($lcat[0] eq "pn" and $per[0] eq "2")||(($lcat[0] eq "pn")&&($gender[0] eq "m")))) {
										$new_gender[0] = "fm";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
									elsif($fmn{$root})
									{
										$new_gender[0] = "fn";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
									elsif($hum{$root})
									{
										$new_gender[0] = "fm";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}

									else
									{
										$new_gender[0] = "n";
										&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
										my $ret_string = &make_string($temp_fs,$sent);
										&modify_field($child,4,$ret_string,$sent);
									}
								}
							}
						}
					}
#					else
#					{
#						$new_gender[0] = "m";
#						&update_attr_val("gen",\@new_gender,$temp_fs,$sent);
#						my $ret_string = &make_string($temp_fs,$sent);
#						&modify_field($child,4,$ret_string,$sent);
#					}
				}
			}
		}
	}
}
&sen_gen;
#$log->info("Printing output");
#
#
if($output eq "" )
{
	&printstory();
}

else
{
	&printstory_file("$output");
}

