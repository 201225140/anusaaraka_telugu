#READING THE INPUT and CONVET IT TO WX
#perl $setu/bin/sys/common/printinput.pl $1 > 
#perl $setu/bin/tl/agrementfeature/tel/remove_at.pl $1 > agreementfeatureinput
perl $setu/bin/tl/agreementfeature/tel/remove_at.pl $1 > agreementfeatureinput

perl $setu/bin/sys/common/convertor-indic-1.4.6/convertor_indic.pl -f=ssf -l=tel -s=utf -t=wx -i=agreementfeatureinput > agreementfeatureinput1
#perl $setu/bin/sys/common/convertor/convertor.pl --path=$setu/bin/sys/common/convertor --stype=ssf --slang=tel -s utf -t wx < agreementfeatureinput > agreementfeatureinput1

#PROCESS THE APPLICATION
perl $setu/bin/tl/agreementfeature/tel/agreementfeature-tel.pl --path=$setu/bin/tl/agreementfeature/tel --male=$setu/bin/tl/agreementfeature/tel/data_src/male-tel-noun  --female=$setu/bin/tl/agreementfeature/tel/data_src/female-tel-noun --human=$setu/bin/tl/agreementfeature/tel/data_src/hum --logFile=temp.log --input=agreementfeatureinput1 > agreementfeatureout

perl $setu/bin/tl/agreementfeature/tel/gen-subj_pn/gen_recon_subjpn.pl --path=$setu/bin/tl/agreementfeature/tel/gen-subj_pn --input=agreementfeatureout >  agreementfeatureout1

perl $setu/bin/tl/agreementfeature/tel/kuch-replace/qcword.pl --path=$setu/bin/tl/agreementfeature/tel/kuch-replace --input=agreementfeatureout1

# Blocked to test the over doing 220312 
# to open this, block the above perl and open the below two codes
#perl $setu/bin/tl/agreementfeature/tel/kuch-replace/qcword.pl --path=$setu/bin/tl/agreementfeature/tel/kuch-replace --input=agreementfeatureout1 --output=agreementfeatureout2
#perl $setu/bin/tl/agreementfeature/tel/case-change.pl --path=$setu/bin/tl/agreementfeature/tel -input=agreementfeatureout2
