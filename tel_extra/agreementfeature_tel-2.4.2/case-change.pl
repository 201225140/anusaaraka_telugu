#!/usr/bin/perl

use Getopt::Long;
GetOptions("help!"=>\$help,"path=s"=>\$vibh_home,"input=s"=>\$input,"output=s",\$output);
print "Unprocessed by Getopt::Long\n" if $ARGV[0];
foreach (@ARGV) {
	print "$_\n";
	exit(0);
}

if($help eq 1)
{
	print "Telugu Generator  - Generator Version 0.91\n(9th Sep 2008 )\n\n";
	print "usage : perl ./tel_gen.pl --path=/home/tel_gen [-i inputfile|--input=\"input_file\"] [-o outputfile|--output=\"output_file\"] \n";
	print "\tIf the output file is not mentioned then the output will be printed to STDOUT\n";
	exit(0);
}

if($vibh_home eq "")
{
	print "Please Specify the Path as defined in --help\n";
	exit(0);

}

require "$vibh_home/API/shakti_tree_api.pl";
require "$vibh_home/API/feature_filter.pl";

open(NOUNDICT,"$vibh_home/data_src/place.txt");
@NOUN=<NOUNDICT>;
chomp(@NOUN);
%noun_root=();
foreach $root_2(@NOUN) {
	($noun,$temp)=split(/,/,$root_2);
	$key=$noun;
	$noun_root{$key}=$temp;
}


sub tel_gen
{
	&read_story($input);
	@nodes1=&get_leaves();

		$cword1 = &get_field($nodes1[0],4);
		$fs_array1 = &read_FS($cword1);
		@drel1[0] = &get_values("drel", $fs_array);


	my @childNodes=&get_children();
	@nodes = (@childNodes,@nodes1);

	for($i=0;$i<=@nodes;$i++)
	{
	$sent=0;

		$cword = &get_field($nodes[$i],4);
		$fs_array = &read_FS($cword);
		@lex = &get_values("cat", $fs_array);
		@case = &get_values("cas", $fs_array);
#print "case = $case[0]\n";
		foreach $lex(@lex)
		{
			if($lex[0] eq "n")
			{
				$cword = $lex;
			}
		}

		if($i!=0)
		{
			$pword1 = &get_field($nodes[$i-1],4);
			$pos1 = &get_field($nodes[$i-1],3);
			$fs_array1 = &read_FS($pword1);
			@lex1 = &get_values("cat", $fs_array1);
			@root1 = &get_values("lex", $fs_array1);
			@cas1 = &get_values("cas", $fs_array1);
			@drel = &get_values("drel", $fs_array1);

			if($drel[0]=~/k1/)
			{
			$drel[0]="k1";

			}

			foreach $lex1(@lex1)
			{
				if($lex1 eq "n")
				{
					$pword1 = $lex1;
				}
			}
			$pword2="";
			$place=$noun_root{$root1[0]};
if(($pword1 eq "n")&&($place eq "yes")&&($cword eq "n")&&($drel[0] ne "k1")) {
					if(($cas1[0] ne "d")&&($pos1!~/^Q/)) {
						$cas1[0]="d";
						@cas1=$cas1[0];
						&update_attr_val("cas", \@cas1, $fs_array1);
						my $string=&make_string($fs_array1);
						&modify_field($nodes[$i-1],4,$string);
					}
				}

			elsif(($pword1 eq "n")&&($place ne "yes")&&($cword eq "n")&&($drel[0] ne "k1")) {
				if(($cas1[0] eq "d")&&($pos1!~/^Q/)) {
					$cas1[0]="o";
					@cas1=$cas1[0];
					&update_attr_val("cas", \@cas1, $fs_array1);
					my $string=&make_string($fs_array1);
					&modify_field($nodes[$i-1],4,$string);
				}

							$place="";

			}

		}

	}

}

&tel_gen();
if($output eq "")
{
	&print_tree();
}
else
{
	&print_tree_file($output);
}
