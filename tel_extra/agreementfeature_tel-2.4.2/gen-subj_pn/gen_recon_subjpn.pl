#!/usr/bin/perl


use Getopt::Long;
GetOptions("help!"=>\$help,"path=s"=>\$vibh_home,"input=s"=>\$input,"output=s",\$output);
print "Unprocessed by Getopt::Long\n" if $ARGV[0];
foreach (@ARGV) {
	print "$_\n";
	exit(0);
}

if($help eq 1)
{
	print "agreement Distribution  Version 1.0\n(5th Oct 2008 )\n\n";
	print "usage : perl ./agreementdistribution.pl --path=\$PWD [-i inputfile|--input=\"input_file\"] [-o outputfile|--output=\"output_file\"] -rule hin/rule.rl\n";
	print "\tIf the output file is not mentioned then the output will be printed to STDOUT\n";
	exit(0);
}

if($vibh_home eq "")
{
	print "Please Specify the Path as defined in --help\n";
	exit(0);

}

require "$vibh_home/API/shakti_tree_api.pl";
require "$vibh_home/API/feature_filter.pl";




&read_story($input);

$numBody = &get_bodycount();
for(my($bodyNum)=1;$bodyNum<=$numBody;$bodyNum++)
{

	$body = &get_body($bodyNum,$body);

	my($numPara) = &get_paracount($body);

	for(my($i1)=1;$i1<=$numPara;$i1++)
	{

		my($para);
		$para = &get_para($i1);


		my($numSent) = &get_sentcount($para);

		for(my($j1)=1;$j1<=$numSent;$j1++)
		{
			$flag = 0;
			my($sent) = &get_sent($para,$j1);
			my @all_children =&get_nodes(3,"VGF",$sent);
			$num = @all_children;
			$flag = 0;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"VGF",$sent);
				$node = $all_children[$i];

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;
				foreach $child (@childs)
				{
					my $pos = &get_field($child, 3,$sent);
					if($pos eq "VM" or $pos eq "VAUX")
					{
						my $val_fs=&get_field($child, 4,$sent);
						$fs_array = &read_FS($val_fs,$sent);
						@gens = &get_values("gen", $fs_array,$sent);
						@rootv = &get_values("lex", $fs_array,$sent);
						if($rootv[0] eq "uMdu"){
							$flagr = "1";
						}
						$flag = 1;
					}
				}
			}


			my @all_children =&get_nodes(3,"NP",$sent);
			$num = @all_children;


			for($j = 0; $j < $num; $j++)
			{
				my @all_children =&get_nodes(3,"NP",$sent);
				$node = $all_children[$j];
				my $fs_data1 = &get_field($node,4,$sent);
				$fs_read1=&read_FS($fs_data1,$sent);
				@drelh = &get_values("drel", $fs_read1,$sent);
				@lcath = &get_values("cat", $fs_read1,$sent);
				@tamn = &get_values("tam", $fs_read1,$sent);


				if(($lcath[0] ne "pn")&&($drelh[0]=~/k1/)&&($tamn[0]!~/^ne/)) {
					my $fs_data2 = &get_field($node,4,$sent);
					$fs_read2=&read_FS($fs_data2,$sent);
					@gend = &get_values("gen", $fs_read2,$sent);
					$flag_1="2";
				}

				if(($lcath[0] eq "pn")&&($drelh[0]=~/k1/)&&($tamn[0]=~/^ne/)) {
					my $fs_data2 = &get_field($node,4,$sent);
					$fs_read2=&read_FS($fs_data2,$sent);
					@gend = &get_values("gen", $fs_read2,$sent);
					$flag_1="3";
				}
				if($drelh[0]=~/k7|k3|k5/){
				$flagn="1";
				}
			}

			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"NP",$sent);
				$node = $all_children[$i];
				my $fs_data1 = &get_field($node,4,$sent);
				$fs_read1=&read_FS($fs_data1,$sent);
				@drel = &get_values("drel", $fs_read1,$sent);
				@lcat = &get_values("cat", $fs_read1,$sent);
				@g = &get_values("gen", $fs_read1,$sent);
				@n= &get_values("num", $fs_read1,$sent);
				#@tam2 = &get_values("vib", $fs_read1,$sent);
				@tam2 = &get_values("tam", $fs_read1,$sent);

				#$gen3[0]="n";
				#my @gen_chunk_arr5=();
				#push @gen_chunk_arr5,$gen3[0];

				my @gen_chunk_arr1=();
				push @gen_chunk_arr1,$gens[0];
				my @gen_chunk_arr2=();
				push @gen_chunk_arr2,$gend[0];
				my @num_chunk_arr=();
				push @num_chunk_arr,$nums[0];
				my @per_chunk_arr=();
				push @per_chunk_arr,$pers[0];


				if(($flag_1 ne "2")&&($flag == 1)&&($lcat[0] eq "pn")&&($drel[0]=~/k1/)&&($tam2[0]!~/^ne/)&&($flagn ne "1")&&($flagr ne "1")&&($g[0]!~/f|m/))
				{
					my $fi1=&get_field($node,4,$sent);
					my $FSreference2 = &read_FS($fi1,$sent);
					&update_attr_val("gen", \@gen_chunk_arr1,$FSreference2,$sent);
					my $string1=&make_string($FSreference2,$sent);
					&modify_field($node,4,$string1,$sent);

				}
			#elsif(($flag_1 ne "2")&&($flag == 1)&&($lcat[0] eq "pn")&&($drel[0]=~/k1/)&&($tam2[0]=~/^ne/))
			#if(($flag_1 eq "3")&&($lcat[0] eq "pn")&&($n[0] eq "pl")&&($drel[0]=~/k1/)&&($tam2[0]=~/^ne/))
			if(($flag_1 eq "3")&&($lcat[0] eq "pn")&&($n[0] eq "pl")&&($drel[0]=~/k1/)&&($tam2[0]=~/^ne/)&&($flagn ne "1")&&($flagr ne "1")) {
						$gens[0]="m";
						my @gen_chunk_arr1=();
						push @gen_chunk_arr1,$gens[0];
					
					my $fi1=&get_field($node,4,$sent);
					my $FSreference2 = &read_FS($fi1,$sent);
					&update_attr_val("gen", \@gen_chunk_arr1,$FSreference2,$sent);
					my $string1=&make_string($FSreference2,$sent);
					&modify_field($node,4,$string1,$sent);

				}
				elsif(($flag_1 eq "2")&&($lcat[0] eq "pn")&&($drel[0]=~/k1/)&&($tam2[0]!~/^ne/)) {
					my $fi2=&get_field($node,4,$sent);
					my $FSreference3 = &read_FS($fi2,$sent);
					&update_attr_val("gen", \@gen_chunk_arr1,$FSreference3,$sent);
					my $string2=&make_string($FSreference3,$sent);
					&modify_field($node,4,$string2,$sent);
				}

				#elsif(($lcat[0] eq "pn")&&($drel[0]!~/k1/)){
				#	my $fi3=&get_field($node,4,$sent);
				#	my $FSreference4 = &read_FS($fi3,$sent);
				#	&update_attr_val("gen", \@gen_chunk_arr5,$FSreference4,$sent);
				#	my $string5=&make_string($FSreference4,$sent);
				#	&modify_field($node,4,$string5,$sent);
				#}

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;

				foreach $child (@childs) {
					my $pos = &get_field($child, 3,$sent);
					if(($pos eq "PRP")&&($drel[0]=~/k1/)&&($lcath[0] eq "pn")&&($tam2[0]!~/^ne/)) {
						my $val_fs1=&get_field($child, 4,$sent);
						$fs_array1 = &read_FS($val_fs1,$sent);
						@lcat = &get_values("cat", $fs_array1,$sent);
						my @gen_chunk_arr=();
						push @gen_chunk_arr,$gens[0];
						my @num_chunk_arr=();
						push @num_chunk_arr,$nums[0];
						my @per_chunk_arr=();
						push @per_chunk_arr,$pers[0];
						my $fi=&get_field($child,4,$sent);
						my $FSreference1 = &read_FS($fi,$sent);
						&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
						my $string=&make_string($FSreference1,$sent);
						&modify_field($child,4,$string,$sent);
					}
					elsif(($pos eq "PRP")&&($drel[0]=~/k1/)&&($lcath[0] eq "pn")&&($tam2[0]=~/^ne/)) {
						my $val_fs1=&get_field($child, 4,$sent);
						$fs_array1 = &read_FS($val_fs1,$sent);
						@lcat = &get_values("cat", $fs_array1,$sent);
						$gens[0]="m";
						my @gen_chunk_arr=();
						push @gen_chunk_arr,$gens[0];
						my @num_chunk_arr=();
						push @num_chunk_arr,$nums[0];
						my @per_chunk_arr=();
						push @per_chunk_arr,$pers[0];
						my $fi=&get_field($child,4,$sent);
						my $FSreference1 = &read_FS($fi,$sent);
						&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
						my $string=&make_string($FSreference1,$sent);
						&modify_field($child,4,$string,$sent);
					}
					elsif(($pos eq "PRP")&&($g[0] ne "n")&&($lcat[0] eq "pn")) {
						my $val_fs1=&get_field($child, 4,$sent);
						my $fs_array1 = &read_FS($val_fs1,$sent);
						my @gens1;
						$gens1[0]="n";
						my @gen_chunk_arr=();
						push @gen_chunk_arr_2,$gens1[0];
						my $fi=&get_field($child,4,$sent);
						my $FSreference1 = &read_FS($fi,$sent);
						&update_attr_val("gen", \@gen_chunk_arr_2,$FSreference1,$sent);
						my $string=&make_string($FSreference1,$sent);
						$string=~s/,nn,/,n,/g;
						&modify_field($child,4,$string,$sent);
					}
				}
			}
			$flagn=""; $flagr="";
		}
	}
}
if($output eq "" )
{
	&printstory();
}

else
{
	&printstory_file("$output");
}

