#!/usr/bin/perl

use Getopt::Long;
GetOptions("help!"=>\$help,"path=s"=>\$vibh_home,"input=s"=>\$input,"output=s"=>\$output);
print "Unprocessed by Getopt::Long\n" if $ARGV[0];
foreach (@ARGV) {
	print "$_\n";
	exit(0);
}

if($help eq 1)
{
	print "JJP_VGF Agreement  -  Version 1.00\n(10th Oct 2009 )\n\n";
	print "usage : perl ./jjpvg_agr.pl --path=\$Path [-i inputfile|--input=\"input_file\"] [-o outputfile|--output=\"output_file\"] \n";
	print "\tIf the output file is not mentioned then the output will be printed to STDOUT\n";
	exit(0);
}

if($vibh_home eq "")
{
	print "Please Specify the Path as defined in --help\n";
	exit(0);

}

require "$vibh_home/API/shakti_tree_api.pl";
require "$vibh_home/API/feature_filter.pl";

sub jjpvg_agen
{
	&read_story($input);
	@nodes1=&get_leaves();

	$cword1 = &get_field($nodes1[0],4);
	$fs_array1 = &read_FS($cword1);
	@gen = &get_values("gen", $fs_array1);
	@num = &get_values("num", $fs_array1);
	@per = &get_values("per", $fs_array1);


	my @childNodes=&get_children();
	@nodes = (@childNodes,@nodes1);

	for($i=0;$i<=@nodes;$i++)
	{
		$sent=0;
		$cword = &get_field($nodes[$i],4);
		$fs_array = &read_FS($cword);
		@root = &get_values("lex", $fs_array);
		@lex = &get_values("cat", $fs_array);
		@gen1 = &get_values("gen", $fs_array);
		@num1 = &get_values("num", $fs_array);
		@per1 = &get_values("per", $fs_array);
		foreach $lex(@lex)
		{

			if($lex[0] eq "v") {
				$root_temp=$root[0];
				$vgen=$gen1[0];
				$vnum=$num1[0];
				$vper=$per1[0];
				$clex = $lex[0];
			}

			if($lex[0] eq "n" or $lex[0] eq "pn") {
				$nlex=$lex[0];
				$ngen=$gen1[0];
				$nnum=$num1[0];
				$nper=$per1[0];
				$nroot=$root[0];
			}
			if($lex[0] eq "adj") {
				$alex=$lex[0];
				$ngen=$gen1[0];
				$nnum=$num1[0];
				$nper=$per1[0];
			}
		}
		if($i!=0) {
			$pword1 = &get_field($nodes[$i-1],4);
			@pos1 = &get_field($nodes[$i-1],3);
			$fs_array1 = &read_FS($pword1);
			@root1 = &get_values("lex", $fs_array1);
			@lex1 = &get_values("cat", $fs_array1);
			@gen2 = &get_values("gen", $fs_array1);
			@num2 = &get_values("num", $fs_array1);
			@vib2 = &get_values("vib", $fs_array1);
			@per2 = &get_values("per", $fs_array1);
			foreach $pos1(@pos1) {
				if($pos1[0]=~/^Q|^NN/) {
					$pos2=$pos1[0];
					$root2=$root1[0];
					$vib2=$vib2[0];

				}
			}
			foreach $lex1(@lex1) {
				if($lex1[0] eq "adj") {
					$plex1 = $lex1[0];
				}
				if($lex1[0] eq "n") {
					$pnlex1 = $lex1[0];
					$pnnum = $num2[0];
					$pngen = $gen2[0];
				}
			}

			if(($root2=~/koVMxaru|koVnni|koVxxi/)&&($nlex eq "n")&&($nnum eq "pl")) {
				if($ngen =~/m|f/){
					$new_root[0]="koVMxaru";
				}
				else{
					$new_root[0]="koVnni";
				}
					&update_attr_val("lex", \@new_root, $fs_array1);
					my $string=&make_string($fs_array1);
					&modify_field($nodes[$i-1],4,$string);
			}
			elsif(($root2=~/aMxaru|aMxarU|aMwA|aMwa|sarvaM/)&&($nlex eq "n")&&($nnum eq "pl")) {
				if($ngen=~/m|f/){
					$new_root[0]="aMxaru";
				}
				else{
					$new_root[0]="anni";
				}
				&update_attr_val("lex", \@new_root, $fs_array1);
				my $string=&make_string($fs_array1);
				&modify_field($nodes[$i-1],4,$string);
			}
			if(($nroot=~/^exEnA/)&&($nlex eq "pn")&&($ngen=~/f|m/)&&($nnum eq "pl")){
				my @new_vib,@new_num;
				$new_vib[0]="eVvaru";
				$new_num[0]="sg";
				&update_attr_val("lex", \@new_vib, $fs_array1);
				&update_attr_val("num", \@new_num, $fs_array1);
				my $string=&make_string($fs_array1);
						&modify_field($nodes[$i],4,$string);
					}
if(($root2!~/oVka|oVkati|reVMdu|mUdu|nAlugu|Exu|Aru|edu/)&&($pos2 eq "QC")&&($ngen=~/f|m/)){
				my @new_vib;
				$new_vib[0]=$vib2."_maMxi";
					
						&update_attr_val("vib", \@new_vib, $fs_array1);
						my $string=&make_string($fs_array1);
						&modify_field($nodes[$i-1],4,$string);
					}


if(($root2=~/^oVka$|^oVkati$|^reVMdu$|^mUdu$|^nAlugu$|^Exu$|^Aru$|^edu$/)&&(($ngen=~/f|m/)||(($nlex eq "pn")&&($ngen eq "n")&&($nper ne "3")))&&($nlex=~/n|pn/)) {
				if(($ngen =~/f|m/)||($nper=~/1|2/)){
				#	$root2=~s/oVka|oVkati/oVkkaru/g;
					$root2=~s/reVMdu/ixxaru/g;
					$root2=~s/mUdu/mugguru/g;
					$root2=~s/nAlugu/naluguru/g;
					$root2=~s/Exu/Exuguru/g;
					$root2=~s/Aru/Aruguru/g;
					$root2=~s/edu/eduguru/g;
					$new_root[0]=$root2;
				}
				else{
					$new_root[0]=$root2;
				}
				&update_attr_val("lex", \@new_root, $fs_array1);
				my $string=&make_string($fs_array1);
				&modify_field($nodes[$i-1],4,$string);
			}
#			if(($nroot=~/eVwanu|exi/)&&($pnlex1 eq "n"))
#			{
#				if($pngen=~/m/){
#					$new_root[0]="eVwanu";
#				}
#				else{
#					$new_root[0]="exi";
#				}
#				$nwcat="pn";
#				my @pnnum1,@newcat;
#				push @pnnum1,$pnnum;
#				push @newcat,$nwcat;
#				&update_attr_val("num", \@pnnum1, $fs_array1);
#				&update_attr_val("lex", \@new_root, $fs_array1);
#				&update_attr_val("cat", \@newcat, $fs_array1);
#				my $string=&make_string($fs_array1);
#				&modify_field($nodes[$i],4,$string);
#			}
			$plex1=""; $root2=""; $nlex=""; $pos2="";
			$clex=""; $pnnum=""; $pngen="";
		}
	}
}

&jjpvg_agen();
if($output eq "") {
	&print_tree();
}
else {
	&print_tree_file($output);
}
