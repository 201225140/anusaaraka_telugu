#!/usr/bin/perl


use Getopt::Long;
GetOptions("help!"=>\$help,"path=s"=>\$vibh_home,"input=s"=>\$input,"output=s",\$output);
print "Unprocessed by Getopt::Long\n" if $ARGV[0];
foreach (@ARGV) {
	print "$_\n";
	exit(0);
}

if($help eq 1)
{
	print "agreement Distribution  Version 1.0\n(5th Oct 2008 )\n\n";
	print "usage : perl ./agreementdistribution.pl --path=\$PWD [-i inputfile|--input=\"input_file\"] [-o outputfile|--output=\"output_file\"] -rule hin/rule.rl\n";
	print "\tIf the output file is not mentioned then the output will be printed to STDOUT\n";
	exit(0);
}

if($vibh_home eq "")
{
	print "Please Specify the Path as defined in --help\n";
	exit(0);

}

require "$vibh_home/API/shakti_tree_api.pl";
require "$vibh_home/API/feature_filter.pl";



#print "here";

&read_story($input);

$numBody = &get_bodycount();
for(my($bodyNum)=1;$bodyNum<=$numBody;$bodyNum++)
{

	$body = &get_body($bodyNum,$body);

	my($numPara) = &get_paracount($body);

	for(my($i1)=1;$i1<=$numPara;$i1++)
	{

		my($para);
		$para = &get_para($i1);


		my($numSent) = &get_sentcount($para);

		for(my($j1)=1;$j1<=$numSent;$j1++)
		{
			$flag = 0;
			my($sent) = &get_sent($para,$j1);
			my @all_children =&get_nodes(3,"NP",$sent);
			$num = @all_children;
			$flag = 0;
			for($i = 0; $i < $num; $i++)
			{
				my @all_children =&get_nodes(3,"NP",$sent);
				$node = $all_children[$i];

				for($q=0;$q<=$node;$q++){
					my $val_fs1=&get_field($node, 4,$sent);
					$fs_array1 = &read_FS($val_fs1,$sent);
						@pncat = &get_values("cat", $fs_array1,$sent);
					#blocked since its gettning conflict with vaha ladakA hE. -chris-170413
					#if(($val_fs1=~/drel=.*k1/)&&($pncat[0] eq "pn")){
					#	$flag_d="1";
					#}
				}
				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;
				foreach $child (@childs)
				{
					my $pos = &get_field($child, 3,$sent);
					my @pos1 = &get_field($child, 3,$sent);
						my $val_fss=&get_field($child, 4,$sent);
						$fs_arrayss = &read_FS($val_fss,$sent);
						@lcatss = &get_values("cat", $fs_arrayss,$sent);
					if(($pos eq "NNP" or $pos eq "PRP" or $pos eq "NN")||(($pos eq "WQ")&&($lcatss[0] eq "pn"))) {
						my $val_fs=&get_field($child, 4,$sent);
						$fs_array = &read_FS($val_fs,$sent);

						@cases = &get_values("vib", $fs_array,$sent);
						@vib = &get_values("case", $fs_array,$sent);
						@lcat = &get_values("cat", $fs_array,$sent);
						@gens = &get_values("gen", $fs_array,$sent);
						@name = &get_values("name", $fs_array,$sent);
#if(($cases[0] eq "0" or $cases[0] eq "\@" or $case[0] eq "\@0" or $cases[0] eq "" or $cases[0] eq "AjFArWa")&&(($lcat[0] eq "n")||($lcat[0] eq "pn"))&&($vib[0] eq "d"))
						if(($cases[0] eq "0" or $cases[0] eq "\@" or $case[0] eq "\@0" or $cases[0] eq "" or $cases[0] eq "AjFArWa")&&(($lcat[0] eq "n")||($lcat[0] eq "pn"))&&($vib[0] ne "o")) {
						if($gens[0] eq "n" or $gens[0] eq "" or $gens[0] eq "any") {
								@gens = &get_values("gen", $fs_array,$sent);
								$flag = 1;
							}
							if($gens[0] eq "f" or $gens[0] eq "fn" or $gens[0] eq "fm" or $gens[0] eq "m") {
								@new_gens = &get_values("gen", $fs_array,$sent);
								if($new_gens[0] eq "m") {
									@gens = &get_values("gen", $fs_array,$sent);
									$flag = 1;
								}
							}
						}
					}
				}
			}

			my @all_children_vnn =&get_nodes(3,"VNN",$sent);
			my @all_children_vgf =&get_nodes(3,"VGF",$sent);
			my @all_children_vgnf =&get_nodes(3,"VGNF",$sent);
			@all_children = (@all_children_vgf, @all_children_vgnf,@all_children_vnn,@all_children_nn);
			$num = @all_children;

		for($i = 0; $i < $num; $i++) {

				$node = $all_children[$i];

				my $headv = &get_field($node, 4,$sent);
				my $headfs = &read_FS($headv,$sent);
				@genv = &get_values("gen", $headfs,$sent);
				@numv = &get_values("num", $headfs,$sent);

				my @gen2=();
				push @gen2,$gens[0];
				if($flag eq "1") {
# correct it and encorp. in the code acroding. to the condition 
# Chris 14-01-2012

$gen2[0]=~s/nn/n/g;
					if(($genv[0] eq "m" and $numv[0] eq "pl")&&($gen2[0] eq "n")){
						&update_attr_val("gen", \@genv,$headfs,$sent);
						my $string=&make_string($headfs,$sent);
						&modify_field($node,4,$string,$sent);
					}
					elsif($flag_d ne "1"){
						&update_attr_val("gen", \@gen2,$headfs,$sent);
						my $string=&make_string($headfs,$sent);
						&modify_field($node,4,$string,$sent);
					}
				}

				my(@childs) = &get_children($node,$sent);
				$num_child = @childs;

				foreach $child (@childs) {
					my $pos = &get_field($child, 3,$sent);
					if($pos eq "VAUX") {
$gens[0]=~s/nn/n/g;
						my @gen_chunk_arr=();
						push @gen_chunk_arr,$gens[0];
						my @num_chunk_arr=();
						push @num_chunk_arr,$nums[0];
						my @per_chunk_arr=();
						push @per_chunk_arr,$pers[0];
						if($flag == 1) {
							my $fi=&get_field($child,4,$sent);
							my $FSreference1 = &read_FS($fi,$sent);
							&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
						#	&update_attr_val("num", \@num_chunk_arr,$FSreference1,$sent);
						#	&update_attr_val("per", \@per_chunk_arr,$FSreference1,$sent);
							my $string=&make_string($FSreference1,$sent);
							&modify_field($child,4,$string,$sent);

						}
					}
					elsif($pos eq "VM") {

$gens[0]=~s/nn/n/g;
						my @gen_chunk_arr=();
						push @gen_chunk_arr,$gens[0];
						my @num_chunk_arr=();
						push @num_chunk_arr,$nums[0];
						my @per_chunk_arr=();
						push @per_chunk_arr,$pers[0];
						if($flag == 1) {
							my $fi=&get_field($child,4,$sent);
							my $FSreference1 = &read_FS($fi,$sent);
							&update_attr_val("gen", \@gen_chunk_arr,$FSreference1,$sent);
							my $string=&make_string($FSreference1,$sent);
							&modify_field($child,4,$string,$sent);
						}
					}
				}
			}
		}
	}
}
if($output eq "" ) {
	&printstory();
}

else {
	&printstory_file("$output");
}

